﻿using ProductMVC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ProductMVC.DAL
{
    public class ProductMVCDBContext : DbContext
    {
        public ProductMVCDBContext()
            :base("name=ProductMVCDBContext")
        {

        }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Product> Products { get; set; }
    }  
}