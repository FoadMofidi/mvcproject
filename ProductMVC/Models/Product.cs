﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProductMVC.Models
{
    public class Product
    {
        public int Id { get; set; }
        [Required]
        [Display(Name ="دسته بندی")]
        public int? CategoryId { get; set; }
        public virtual Category Category { get; set; }
        [Required]
        [Display(Name = "کمپانی")]
        public int CompanyId { get; set; }
        public virtual Company Company { get; set; }
        [Required]
        [Display(Name = "نام محصول")]
        public string ModelName { get; set; }
        [Display(Name = "توضیحات")]
        public string Description { get; set; }
    }
}